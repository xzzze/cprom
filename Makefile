# CPROM
include config.mk

CC ?= gcc
CFLAGS += -fPIC -Wall -Wextra -I"src/" -march=native
ifndef DEBUG
	ADDITIONAL_CFLAGS ?= -O3
	SANITIZE =
else
	ADDITIONAL_CFLAGS ?= -Og -g3
	SANITIZE = -fsanitize=address,undefined
endif
CFLAGS += $(ADDITIONAL_CFLAGS)

ifdef ZSH
	ADDITIONAL_CFLAGS+= -DANSI_ZSH
endif

RM ?= rm -f

MODULES_SRCS = $(addsuffix .c, $(addprefix src/modules/, $(MODULES)))

SRCS = $(wildcard $(SRC)/*.c)
SRCS += $(MODULES_SRCS)

OBJS = $(patsubst $(SRC)/%.c, $(OBJ)/%.o, $(SRCS))

LDFLAGS += -lm $(SANITIZE)

ifneq (,$(findstring git,$(MODULES)))
	LDFLAGS += -lgit2
endif

all: options cprom

options:
	@echo cprom build options:
	@echo "CC         = $(CC)"
	@echo "CFLAGS     = $(CFLAGS)"
	@echo "LDFLAGS    = $(LDFLAGS)"
	@echo "MODULES    = $(MODULES)"
	@echo "SANITIZERS = $(SANITIZE)"


$(OBJ)/%.o: $(SRC)/%.c
	test -f $@ || mkdir -p $@ && rm -r $@ # this is a stupid
	$(CC) -c $(CFLAGS) $< -o $@

cprom: $(OBJS)
	$(CC) -o $@ $(OBJS) $(LDFLAGS)

install:
	install -d $(PREFIX)/bin
	install -m 755 cprom $(PREFIX)/bin/cprom
	install -d $(MANPREFIX)/man1
	install -m 755 doc/cprom.1 $(MANPREFIX)/man1/cprom.1

clean:
	$(RM) $(OBJS) cprom
