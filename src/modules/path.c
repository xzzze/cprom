#include "path.h"

#include <stdlib.h>
#include <string.h>

#include <pwd.h>
#include <sys/types.h>
#include <unistd.h>

#define PathBufLen 1024
int GetFullPath(char **result, void *args) {
  *result = malloc(PathBufLen);
  if (getcwd(*result, PathBufLen) != *result) {
    memset(*result, 0, PathBufLen);
    strcpy(*result, "???");
    return 0;
  }
  struct passwd *pw = getpwuid(getuid());
  const char *homedir = pw->pw_dir;

  if (strncmp(*result, homedir, strlen(homedir)) == 0) {
    const char *temp = strdup(*result + strlen(homedir));
    memset(*result, 0, PathBufLen);
    strcpy(*result, "~");
    strcpy(*result + 1, temp);
    free(temp);
  }

  return 0;
}
int GetDir(char **result, void *args) {
  char *path = NULL;
  GetFullPath(&path, NULL);
  char *c = path + strlen(path) - 1;

  while (*(c-- - 1) != '/' || *(c - 1) != '~')
    ;
  c++;

  *result = strdup(c);
  return 0;
}
