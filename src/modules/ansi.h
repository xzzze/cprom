#ifndef INCLUDE_ANSI_H
#define INCLUDE_ANSI_H


#ifdef ANSI_ZSH
#define ANSI_RESET     "%{\e[0m%}"
#define ANSI_BOLD      "%{\e[1m%}"
#define ANSI_ITALICS   "%{\e[3m%}"
#define ANSI_UNDERLINE "%{\e[4m%}"

#define ANSI_BLACK     "%{\e[30m%}"
#define ANSI_RED       "%{\e[31m%}"
#define ANSI_GREEN     "%{\e[32m%}"
#define ANSI_YELLOW    "%{\e[33m%}"
#define ANSI_BLUE      "%{\e[34m%}"
#define ANSI_PURPLE    "%{\e[35m%}"
#define ANSI_CYAN      "%{\e[36m%}"
#define ANSI_WHITE     "%{\e[37m%}"

#else
#define ANSI_RESET     "\e[0m"
#define ANSI_BOLD      "\e[1m"
#define ANSI_ITALICS   "\e[3m"
#define ANSI_UNDERLINE "\e[4m"

#define ANSI_BLACK     "\e[30m"
#define ANSI_RED       "\e[31m"
#define ANSI_GREEN     "\e[32m"
#define ANSI_YELLOW    "\e[33m"
#define ANSI_BLUE      "\e[34m"
#define ANSI_PURPLE    "\e[35m"
#define ANSI_CYAN      "\e[36m"
#define ANSI_WHITE     "\e[37m"
#endif
#endif
