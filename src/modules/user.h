#ifndef INCLUDE_USER_H
#define INCLUDE_USER_H

int GetUserName(char **, void *);
int GetHostName(char **, void *);
int GetName(char **, void *);

#endif
