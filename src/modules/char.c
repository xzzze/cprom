#include "char.h"

#include "ansi.h"
#include <string.h>
#include <stdlib.h>

int GetChar(char ** result, void * args) {
  CharData *data = (CharData*) args;
  size_t len = strlen(data->ch);
  char *start;
  
  if(data->success_color == NULL) {
    data->success_color = ANSI_ITALICS ANSI_GREEN;
  }

  if(data->failure_color == NULL) {
    data->failure_color = ANSI_BOLD ANSI_RED;
  }

  if (*(data->retval) == 0) {
    start = data->success_color;
  } else {
    start = data->failure_color;
  }
  const char *end = ANSI_RESET;
  *result = malloc(len + strlen(start) + strlen(end) + 1);
  strcpy(*result, start);
  strcpy(*result+strlen(start), data->ch);
  strcpy(*result+strlen(start)+len, end);

  return 0;
}
