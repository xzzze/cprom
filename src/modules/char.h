#ifndef INCLUDE_CHAR_H
#define INCLUDE_CHAR_H

typedef struct CharData {
  int *retval;
  const char *ch;
  char *success_color;
  char *failure_color;
} CharData;

int GetChar(char **, void *);

#endif
