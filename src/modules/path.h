#ifndef INCLUDE_PATH_H
#define INCLUDE_PATH_H

int GetFullPath(char **, void *);
int GetDir(char **, void *);

#endif
