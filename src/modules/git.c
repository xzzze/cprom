#include "git.h"

#include <git2.h>

#include <string.h>
#include <unistd.h>

int started = 0;

void startlibgit2() {
  if (!started) {
    git_libgit2_init();
    started = 1;
  }
}

#define PathBufLen 1024
int GetBranch(char **result, void *args) {
  int error = 0;
  startlibgit2();

  char *path = malloc(PathBufLen);
  getcwd(path, PathBufLen);

  git_repository *repo = NULL;
  error = git_repository_open(&repo, path);
  if (error != 0) {
    return -1;
  }

  git_reference *head_ref;
  error = git_reference_lookup(&head_ref, repo, "HEAD");
  if (error != 0) {
    git_repository_free(repo);
    free(path);
    return -1;
  }

  const char *symbolic_ref;
  symbolic_ref = git_reference_symbolic_target(head_ref);
  *result = strdup(symbolic_ref + 11);
  git_reference_free(head_ref);

  git_repository_free(repo);
  free(path);
  return 0;
}
