#include "user.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <errno.h>

int GetUserName(char **result, void * args) {
  char *l = getlogin();
  if (l == NULL)
    return -1;

  *result = strdup(l);
  return 0;
}

#define HostNameBufSize 1024
int GetHostName(char **result, void *args) {
  *result = malloc(HostNameBufSize);

  if (gethostname(*result, HostNameBufSize) == 0) {
    return 0;
  }

  return -1;
}

#define GetNameFormat "%s@%s"
int GetName(char **result, void * args) {
  char *username = NULL, *hostname = NULL;
  GetUserName(&username, NULL);
  GetHostName(&hostname, NULL);

  size_t size = snprintf(NULL, 0, GetNameFormat, username, hostname)+1;
  *result = malloc(size);
  sprintf(*result, GetNameFormat, username, hostname);

  free(username);
  free(hostname);
  return 0;
}
