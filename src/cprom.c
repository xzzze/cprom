#include "cprom.h"
#include <stdio.h>
#include <stdlib.h>

#include "../config.h"

void load_parts(const FormatPart parts[], size_t *p_amount, char ***partstrings, int **freeable);

int main(int argc, char **argv) {
  if (argc >= 2)
    retval = atoi(argv[1]);
  else
    retval = 0;

  size_t amount = 0;

  char **partstrings;
  int *freeable;

#ifdef ANSI_ZSH
  if (argc >= 3)
    load_parts(rprompt_parts, &amount, &partstrings, &freeable);
  else
#endif
  load_parts(prompt_parts, &amount, &partstrings, &freeable);

  for (size_t i = 0; i < amount; i++) {
    printf("%s", partstrings[i]);
    if (freeable[i])
      free(partstrings[i]);
  }
  free(partstrings);
  return 0;
}

void load_parts(const FormatPart parts[], size_t *p_amount, char ***partstrings, int **freeable) {
  size_t amount = 0;
  for (amount = 0; parts[amount].formatstring != NULL; amount++)
    ;

  *partstrings = malloc(sizeof(char *) * amount);
  *freeable = malloc(sizeof(int) * amount);
  for (size_t i = 0; i < amount; i++) {
    FormatPart part = parts[i];
    if (part.fn == NULL) {
      (*partstrings)[i] = (char *)part.formatstring;
      (*freeable)[i] = 0;
    } else {
      (*freeable)[i] = 1;
      char *res = NULL;
      int err = (part.fn)((&res), part.args);
      const size_t size = snprintf(NULL, 0, part.formatstring, res) + 1;
      (*partstrings)[i] = malloc(size);
      if (err == 0) {
        snprintf((*partstrings)[i], size, part.formatstring, res);
      } else {
        (*partstrings)[i] = malloc(1);
        (*partstrings)[i][0] = 0;
      }
      free(res);
    }
  }
  *p_amount = amount;
}
