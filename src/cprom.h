#ifndef INCLUDE_CPROM_H
#define INCLUDE_CPROM_H

typedef int (*PartFunc)(char **, void *);

typedef struct {
  const char *formatstring;
  PartFunc fn;
  void *args;
} FormatPart;

int main();

#endif
