#include "cprom.h"

#include "modules/ansi.h"
#include "modules/char.h"
#include "modules/path.h"
#include "modules/user.h"
#include "modules/git.h"

#include <stdlib.h>

static int retval;

static CharData chrdata = {
  .retval = &retval,
  .ch = "$ ",
  .success_color = ANSI_ITALICS ANSI_GREEN,
  .failure_color = ANSI_BOLD ANSI_RED,
};

static const FormatPart prompt_parts[] = {
  {ANSI_RESET                  ANSI_YELLOW       "[",          NULL,      NULL,},
  {ANSI_RESET    ANSI_BOLD     ANSI_PURPLE     "%s ",       GetName,      NULL,},
  {ANSI_RESET    ANSI_ITALICS  ANSI_BLUE        "%s",   GetFullPath,      NULL,},
  {ANSI_RESET                  ANSI_YELLOW      "] ",          NULL,      NULL,},
  {ANSI_RESET                                   "%s",       GetChar,  &chrdata,},

  {NULL,NULL,NULL}
};

#ifdef ANSI_ZSH
static const FormatPart rprompt_parts[] = {
  {ANSI_RESET ANSI_ITALICS ANSI_PURPLE "{%s}" ANSI_RESET,       GetBranch,      NULL},
  {NULL,NULL,NULL}
};
#endif

