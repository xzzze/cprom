# CPROM - A Simple Unix Shell Prompt Written in C

## How to Use
### ZSH
Compile with: `make ZSH=1`  
Add this to your .zshrc: `PROMPT='$(cprom $?)'`  
If you want to have a right prompt then this to your .zshrc: `RPROMPT='$(cprom $? -)'`
### Bash
Add this to your .bashrc: `PROMPT='$(cprom $?)'`

## Configuration
Configuration is done in the `config.h` header file. You have to have a `prompt_parts[]` array with the type `FormatPart`.
You can also have a `rprompt_parts[]` array where you can configure the appearance of your right sided prompt.
The `FormatPart` struct contains 3 values, a format string, a function that generates the single string argument for the
format string, and a argument to a function. The arrays have to end with `{NULL,NULL,NULL}`. An example config can be found
in `config.h`.
